module.exports = {
  publicPath: '/app/',
  lintOnSave: false,
  devServer: {
    port: 3454
  }
}
