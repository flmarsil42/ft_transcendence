import { UserPublicPOV } from "./userPublicPOV";

export interface DirectMessageThread {
    id: number
    lastEmitterLogin: string
    lastEmitterUsername: string
    lastMessageSeen: boolean
    updatedAt: Date
    user1: UserPublicPOV;
    user2: UserPublicPOV;
}