export interface BasicResponseObjectFromBack {
    server: string;
    date: string;
    login: string;
    status: BasicResponseStatus;
    data?: Object;
    error?: ErrorOBJ;
}

export enum BasicResponseStatus {
    OK = 'OK',
    ERROR = 'ERROR',
}

interface ErrorOBJ {
    errorDescription: string;
    errorId: number;
}