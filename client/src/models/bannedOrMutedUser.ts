export interface BannedOrMutedUser {
    id: number;

    userLogin: string;

    state: BannedOrMutedUserStatus;

    updatedAt: Date;

    expiryDate: Date;
}

export enum BannedOrMutedUserStatus {
    BANNED = 'BANNED',
    MUTED = 'MUTED',
}