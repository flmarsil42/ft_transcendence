export interface UserPublicPOV {
  login: string;
  username: string;
  avatar: string;
  wins: number;
  loses: number;
  ratio: number;
  xp: number;
  lvl: number;
  //0: offline, 1: online, 2: do not disturb
  status: number;
  playingCurrently: boolean;
}
