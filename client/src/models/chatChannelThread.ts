import { UserPublicPOV } from "./userPublicPOV";

export interface ChatChannelThread {
    id: Number;
    createdAt: string;
    chatChannelName: string;
    privateChatChannel: boolean;
    owner: UserPublicPOV;
}