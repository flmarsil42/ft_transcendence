import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

// import jQuery from "jquery";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./style/mycss.css";

createApp(App)
  .use(router)
  // .use(jQuery)
  .mount("#app");
