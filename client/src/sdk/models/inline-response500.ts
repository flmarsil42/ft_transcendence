/* tslint:disable */
/* eslint-disable */
/**
 * ft_transcendence_api
 * This is a ft_transcendence Api.  You can find out more about     ft_transcendence at [github](http://github.com/FouadAssani/ft_transcendence_API)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: assani@asimovcorp.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface InlineResponse500
 */
export interface InlineResponse500 extends BasicResponseObject {
    /**
     * 
     * @type {string}
     * @memberof InlineResponse500
     */
    result?: InlineResponse500ResultEnum;
    /**
     * 
     * @type {ServerError500Error}
     * @memberof InlineResponse500
     */
    error: ;
}

/**
    * @export
    * @enum {string}
    */
export enum InlineResponse500ResultEnum {
    ERROR = 'ERROR'
}

