/* tslint:disable */
/* eslint-disable */
/**
 * ft_transcendence_api
 * This is a ft_transcendence Api.  You can find out more about     ft_transcendence at [github](http://github.com/FouadAssani/ft_transcendence_API)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: assani@asimovcorp.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import globalAxios, { AxiosPromise, AxiosInstance } from 'axios';
import { Configuration } from '../configuration';
// Some imports not used depending on template conditions
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from '../base';
import { InlineResponse200 } from '../models';
import { InlineResponse2001 } from '../models';
import { InlineResponse500 } from '../models';
/**
 * UserApi - axios parameter creator
 * @export
 */
 globalAxios.defaults.withCredentials = true;
export const UserApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * The avatar file must be in format .jpg. This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user's avatar.
         * @param {string} [file] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeAccountAvatar: async (file?: string, options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/user/avatar`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;
            const localVarFormParams = new FormData();


            if (file !== undefined) { 
                localVarFormParams.append('file', file as any);
            }

            localVarHeaderParameter['Content-Type'] = 'multipart/form-data';
            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            localVarRequestOptions.data = localVarFormParams;

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user status(offline, online, do not disturb).
         * @param {number} status 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeUserStatus: async (status: number, options: any = {}): Promise<RequestArgs> => {
            // verify required parameter 'status' is not null or undefined
            if (status === null || status === undefined) {
                throw new RequiredError('status','Required parameter status was null or undefined when calling changeUserStatus.');
            }
            const localVarPath = `/user/status`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (status !== undefined) {
                localVarQueryParameter['status'] = status;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the username of an user.
         * @param {string} username 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeUsername: async (username: string, options: any = {}): Promise<RequestArgs> => {
            // verify required parameter 'username' is not null or undefined
            if (username === null || username === undefined) {
                throw new RequiredError('username','Required parameter username was null or undefined when calling changeUsername.');
            }
            const localVarPath = `/user/username`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (username !== undefined) {
                localVarQueryParameter['username'] = username;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. Note: If a user is deleted from database, the directMessageThreads in which the user is involved is deleted, and all the directMessages related are also deleted. 
         * @summary To delete a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        disableAccount: async (options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/user`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'DELETE', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To get the information on a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getAccountInfo: async (options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/user`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To disconnect a user from the page.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        logoutAccount: async (options: any = {}): Promise<RequestArgs> => {
            const localVarPath = `/user/logout`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the authentication factor mode.
         * @param {boolean} activate 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        twoFactorAuth: async (activate: boolean, options: any = {}): Promise<RequestArgs> => {
            // verify required parameter 'activate' is not null or undefined
            if (activate === null || activate === undefined) {
                throw new RequiredError('activate','Required parameter activate was null or undefined when calling twoFactorAuth.');
            }
            const localVarPath = `/user/twoFactorAuth`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            if (activate !== undefined) {
                localVarQueryParameter['activate'] = activate;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.query) {
                query.set(key, options.query[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * UserApi - functional programming interface
 * @export
 */
export const UserApiFp = function(configuration?: Configuration) {
    return {
        /**
         * The avatar file must be in format .jpg. This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user's avatar.
         * @param {string} [file] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async changeAccountAvatar(file?: string, options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).changeAccountAvatar(file, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user status(offline, online, do not disturb).
         * @param {number} status 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async changeUserStatus(status: number, options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).changeUserStatus(status, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the username of an user.
         * @param {string} username 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async changeUsername(username: string, options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).changeUsername(username, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. Note: If a user is deleted from database, the directMessageThreads in which the user is involved is deleted, and all the directMessages related are also deleted. 
         * @summary To delete a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async disableAccount(options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).disableAccount(options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To get the information on a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async getAccountInfo(options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse2001>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).getAccountInfo(options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To disconnect a user from the page.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async logoutAccount(options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).logoutAccount(options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the authentication factor mode.
         * @param {boolean} activate 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async twoFactorAuth(activate: boolean, options?: any): Promise<(axios?: AxiosInstance, basePath?: string) => AxiosPromise<InlineResponse200>> {
            const localVarAxiosArgs = await UserApiAxiosParamCreator(configuration).twoFactorAuth(activate, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
    }
};

/**
 * UserApi - factory interface
 * @export
 */
export const UserApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    return {
        /**
         * The avatar file must be in format .jpg. This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user's avatar.
         * @param {string} [file] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeAccountAvatar(file?: string, options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).changeAccountAvatar(file, options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the user status(offline, online, do not disturb).
         * @param {number} status 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeUserStatus(status: number, options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).changeUserStatus(status, options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the username of an user.
         * @param {string} username 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        changeUsername(username: string, options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).changeUsername(username, options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. Note: If a user is deleted from database, the directMessageThreads in which the user is involved is deleted, and all the directMessages related are also deleted. 
         * @summary To delete a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        disableAccount(options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).disableAccount(options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To get the information on a user account.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        getAccountInfo(options?: any): AxiosPromise<InlineResponse2001> {
            return UserApiFp(configuration).getAccountInfo(options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To disconnect a user from the page.
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        logoutAccount(options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).logoutAccount(options).then((request) => request(axios, basePath));
        },
        /**
         * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
         * @summary To change the authentication factor mode.
         * @param {boolean} activate 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        twoFactorAuth(activate: boolean, options?: any): AxiosPromise<InlineResponse200> {
            return UserApiFp(configuration).twoFactorAuth(activate, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * UserApi - object-oriented interface
 * @export
 * @class UserApi
 * @extends {BaseAPI}
 */
export class UserApi extends BaseAPI {
    /**
     * The avatar file must be in format .jpg. This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To change the user's avatar.
     * @param {string} [file] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public changeAccountAvatar(file?: string, options?: any) {
        return UserApiFp(this.configuration).changeAccountAvatar(file, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To change the user status(offline, online, do not disturb).
     * @param {number} status 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public changeUserStatus(status: number, options?: any) {
        return UserApiFp(this.configuration).changeUserStatus(status, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To change the username of an user.
     * @param {string} username 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public changeUsername(username: string, options?: any) {
        return UserApiFp(this.configuration).changeUsername(username, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. Note: If a user is deleted from database, the directMessageThreads in which the user is involved is deleted, and all the directMessages related are also deleted. 
     * @summary To delete a user account.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public disableAccount(options?: any) {
        return UserApiFp(this.configuration).disableAccount(options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To get the information on a user account.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public getAccountInfo(options?: any) {
        return UserApiFp(this.configuration).getAccountInfo(options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To disconnect a user from the page.
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public logoutAccount(options?: any) {
        return UserApiFp(this.configuration).logoutAccount(options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * This operation can only be performed by a user that is already connected. The login is fetched automatically from the cookie. 
     * @summary To change the authentication factor mode.
     * @param {boolean} activate 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof UserApi
     */
    public twoFactorAuth(activate: boolean, options?: any) {
        return UserApiFp(this.configuration).twoFactorAuth(activate, options).then((request) => request(this.axios, this.basePath));
    }
}
