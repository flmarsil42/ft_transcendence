interface dataUser {
  id: number;
  login: string;
  username: string;
  email: string;
  avatar: string;
  wins: number;
  loses: number;
  ratio: number;
  xp: number;
  lvl: number;
  status: number;
  playingCurrently: boolean;
  twoFactorAuth: boolean;
  createdAt: string;
  isAdmin: boolean;
}

export default dataUser;
