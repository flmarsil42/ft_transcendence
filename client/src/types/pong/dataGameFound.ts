interface dataGameFound {
  gameId: string;
  player1: string;
  player2: string;
  scorePlayer1: number;
  scorePlayer2: number;
  mapName: string;
  isWatched: boolean;
}

export default dataGameFound;
