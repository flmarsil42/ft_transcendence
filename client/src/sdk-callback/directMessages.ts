import { DirectMessagesThreadBody } from "../sdk/models";

let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiChat = new FtTranscendenceApi.DirectMessagesApi();

// to update the lastMessageSeen boolean to true, on a given direct messages thread.
export async function directMessageThreadUpdateOne(
  body: DirectMessagesThreadBody
) {
  let response = await apiChat.directMessageThreadUpdateOne(body);
  return response.data;
}

// to get the list of the directMessageThreads for a user (most recent thread first).
export async function getListOfDirectMessageThreads(
  limit?: number,
  offset?: number
) {
  let response = await apiChat.getListOfDirectMessageThreads(limit, offset);
  return response.data;
}

// to get the list of the directMessageThreads for a user (most recent thread first).
export async function getDirectMessagesThreadExists(
  username: string,
) {
  let response = await apiChat.getDirectMessagesThreadExists(username);
  return response.data;
}

// to get the list of the directMessages in a Thread for a user (most recent message first).
export async function getListOfDirectMessagesInThread(
  threadId: Number,
  limit?: Number,
  offset?: Number
) {
  let response = await apiChat.getListOfDirectMessagesInThread(
    threadId,
    limit,
    offset
  );
  return response.data;
}
