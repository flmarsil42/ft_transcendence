let FtTranscendenceApi = require('ft_transcendence_sdk');
let apiChat = new FtTranscendenceApi.ChatChannelsApi();

// To add an admin to a chat channel.
export async function adminAddToChatChannel(loginNewAdmin: string, idChannel: number) {
    let response = await apiChat.adminAddToChatChannel(loginNewAdmin, idChannel)
    return response.data
}

// To remove an admin from a chat channel.
export async function adminRemoveFromChatChannel(idChannel: string, loginTargetAdmin: string) {
    let response = await apiChat.adminRemoveFromChatChannel(idChannel, loginTargetAdmin)
    return response.data
}

//To change the password of a chat channel.
export async function changePasswordInChatChannel(password: string, idChannel: string) {
    let response = await apiChat.changePasswordInChatChannel(password, idChannel)
    return response.data
}

//To create a chat channel.
export async function createOneChatChannel(privateChatChannel: boolean, password?: string, chatChannelName?: string) {
    let response = await apiChat.createOneChatChannel(privateChatChannel, password, chatChannelName)
    return response.data
}

//To delete a chatChannel.
export async function deleteOneChatChannel(idChannel: string) {
    let response = await apiChat.deleteOneChatChannel(idChannel)
    return response.data
}


/* ******** LIST MEMBERS IN CHAT CHANNEL ******** */

//To get a list of the admin profiles within a chat channel.
export async function getListOfAdminProfilesInChatChannel(idChannel: string, limit?: number) {
    let response = await apiChat.getListOfAdminProfilesInChatChannel(idChannel, limit)
    return response.data
}

//To get a list of the regular guest profiles within a chat channel.
export async function getListOfGuestProfilesInChatChannel(idChannel: string, limit?: number, offset?: number) {
    let response = await apiChat.getListOfGuestProfilesInChatChannel(idChannel, limit, offset)
    return response.data
}

//To get a list of the profiles within a chat channel.
export async function getListOfAllProfilesInChatChannel(idChannel: string, limit?: number, offset?: number) {
    let response = await apiChat.getListOfAllProfilesInChatChannel(idChannel, limit, offset)
    return response.data
}


/* ******** LIST OF CHAT CHANNELS ******** */

//To get a list of all the chatChannels in which the connected user is a member.
export async function getListOfChatChannelsAsAnyMember(privacy: string, limit?: number,) {
    let response = await apiChat.getListOfChatChannelsAsAnyMember(privacy, limit)
    return response.data
}

//To get a list of all the chatChannels the user is owning.
export async function getListOfChatChannelsAsOwner(privacy: string, limit?: number) {
    let response = await apiChat.getListOfChatChannelsAsOwner(privacy, limit)
    return response.data
}

//To get a list of the public chat channels.
export async function getListOfPublicChatChannels(limit?: number, offset?: number) {
    let response = await apiChat.getListOfPublicChatChannels(limit, offset)
    return response.data
}


/* ******** ADD / JOIN /QUIT CHATCHANNEL ******** */

//To add another member to a private chat channel.
export async function inviteMemberToPrivateChatChannel(idChannel: string, loginTarget: string, password?: string) {
    let response = await apiChat.inviteMemberToPrivateChatChannel(idChannel, loginTarget, password)
    return response.data
}

//To join a chat channel.
export async function joinPublicChatChannel(idChannel: string, password?: string) {
    let response = await apiChat.joinPublicChatChannel(idChannel, password)
    return response.data
}

//To quit a chat channel.
export async function quitChatChannel(login: string, idChannel: string) {
    let response = await apiChat.quitChatChannel(login, idChannel)
    return response.data
}


/* ******** BAN/MUTE IN CHAT CHANNEL ******** */

/* ############# BANS ############### */
//To ban a user from a chat channel.
export async function banUserInChatChannel(loginTarget: string, idChannel: string, durationInSeconds: number) {
    let response = await apiChat.banUserInChatChannel(loginTarget, idChannel, durationInSeconds)
    return response.data
}

//To try to remove a ban sanction on an user, for a given chatChannel.
export async function banUserInChatChannelUndo(loginTarget: string, idChannel: string) {
    let response = await apiChat.banUserInChatChannelUndo(loginTarget, idChannel)
    return response.data
}

//to get a list of the bannedUsers in the given chatChannel.
export async function getListOfBannedUsersInChatChannel(idChannel: string) {
    let response = await apiChat.getListOfBannedUsersInChatChannel(idChannel)
    return response.data
}

/* ############# MUTES ############### */
//To mute a user within a chat channel.
export async function muteUserInChatChannel(loginTarget: string, idChannel: string, durationInSeconds: number) {
    let response = await apiChat.muteUserInChatChannel(loginTarget, idChannel, durationInSeconds)
    return response.data
}

//To try to remove a mute sanction on an user within a given chatChannel.
export async function muteUserInChatChannelUndo(loginTarget: string, idChannel: string) {
    let response = await apiChat.muteUserInChatChannelUndo(loginTarget, idChannel)
    return response.data
}

//to get a list of the muted Users in the given chatChannel.
export async function getListOfMutedUsersInChatChannel(idChannel: string) {
    let response = await apiChat.getListOfMutedUsersInChatChannel(idChannel)
    return response.data
}