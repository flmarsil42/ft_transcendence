let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiFriends = new FtTranscendenceApi.FriendsApi();

// accept contact request to become friend with someone
export async function acceptFriend(username: string) {
  let response = await apiFriends.acceptFriend(username);
  return response.data;
}

// delete friend from list
export async function deleteContactUser(username: string) {
  let response = await apiFriends.deleteContactUser(username);
  return response.data;
}

// get list of request to become friends with someone
export async function getContactRequest(index: number) {
  let response = await apiFriends.getContactRequest(index);
  return response.data;
}

// get list of friends
export async function getListOfFriends(index: number) {
  let response = await apiFriends.getListOfFriends(index);
  return response.data;
}

// refuse contact request to become friend with someone
export async function refuseFriend(username: string) {
  let response = await apiFriends.refuseFriend(username);
  return response.data;
}

// send contact request to someone to become friend
export async function setContactUser(username: string) {
  let response = await apiFriends.setContactUser(username);
  return response.data;
}
