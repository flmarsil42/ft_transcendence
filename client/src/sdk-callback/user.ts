let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiUser = new FtTranscendenceApi.UserApi();

export async function getAccountInfo() {
  let response = await apiUser.getAccountInfo();
  return response.data.data;
}

export async function changeAccountAvatar(file: any) {
  return await apiUser.changeAccountAvatar(file);
}

export async function changeUsername(newUsername: string) {
  return await apiUser.changeUsername(newUsername);
}

export async function changeUserStatus(newStatus: number) {
  return await apiUser.changeUserStatus(newStatus);
}

export async function disableAccount() {
  return await apiUser.disableAccount();
}

export async function logoutAccount() {
  return await apiUser.logoutAccount();
}

export async function twoFactorAuth(activate: boolean) {
  return await apiUser.twoFactorAuth(activate);
}
