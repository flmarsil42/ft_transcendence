let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiApi = new FtTranscendenceApi.ApiApi();

// get status connection of user
export async function getIsconnected() {
  let response = await apiApi.getIsconnected();
  return response.data.data;
}

export async function getLogin() {
  let response = await apiApi.getLogin();
  return response.data.data;
}

export async function put2faCode(authCode: string) {
  let response = await apiApi.put2faCode(authCode);
  return response.data.data;
}
