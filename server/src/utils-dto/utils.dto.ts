import { extname } from "path";
import { Any } from "typeorm";

export enum ResponseStatus {
  OK = "OK",
  ERROR = "ERROR",
}

export interface ErrorOBJ {
  errorDescription: string;
  errorId: number;
}

export interface ResponseOBJ {
  server: string;
  date: string;
  status: ResponseStatus;
  login: string;
  data?: Object;
  error?: ErrorOBJ;
}

export function addErrorToResponseObj(
  responseObj: ResponseOBJ,
  errorDescription: string,
  errorId: number
) {
  let newError: ErrorOBJ = {
    errorDescription: errorDescription,
    errorId: errorId,
  };
  responseObj.status = ResponseStatus.ERROR;
  responseObj.error = newError;
  //console.log("\x1b[31m%s\x1b[0m", errorDescription);
}

export function addInternalServerErrorToResponseObj(responseObj: ResponseOBJ) {
  let newError: ErrorOBJ = {
    errorDescription: "internal server errror",
    errorId: 0,
  };
  responseObj.status = ResponseStatus.ERROR;
  responseObj.error = newError;
}

export function initResponse(query: {}): ResponseOBJ {
  let dateTime = new Date();

  /*
    let error: ErrorOBJ = {
        errorDescription: '',
        errorCode: 0
    }
    */

  let res: ResponseOBJ = {
    server: "ft_transcendence/1.0",
    date: dateTime.toISOString(),
    status: ResponseStatus.OK,
    login: "",
    //data: {},
    //error: error
  };
  if (query.hasOwnProperty("login")) res.login = query["login"];
  return res;
}

export const editFileName = (req, file, callback) => {
  let crypto = require("crypto");

  const randomName = crypto.randomBytes(30).toString("hex");
  const login = req.query["login"];
  callback(null, `${login}_${randomName}.jpg`);
};

export const imageFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg)$/)) {
    req.query["errorFile"] = "error";
    return callback(null, false);
  }
  callback(null, true);
};

export async function download(url, dest) {
  const http = require("https");
  const fs = require("fs");
  //console.log("----------------   telechargement de l image :", url);
  const request = http.get(url, function (response) {
    if (response.statusCode === 200) {
      //console.log("----------------    image telechargé :", dest);
      var file = fs.createWriteStream("./uploadstore/" + dest);
      response.pipe(file);
    }
    request.setTimeout(60000, function () {
      // if after 60s file not downlaoded, we abort a request
      request.abort();
    });
  });
}

export async function uploadstore_deletefile(name: string): Promise<Number> {
  const fs = require("fs");
  try {
    let tmp = name.replace("http://localhost:8080/avatar/", "")
    await fs.unlink("./uploadstore/" + tmp, (err) => {
      //permet de supprimer l'ancien avatar
      if (err) {
        console.error(err);
        return 0;
      } else {
        return 1;
      }
    });
  } catch {
    //console.log("cookieOBJ undefined");
  }

  return 1;
}

export async function isUserNameValid(username: string) {
  /* 
      Usernames can only have: 
      - Lowercase Letters (a-z) 
      - Numbers (0-9)
      - Dots (.)
      - Underscores (_)
      - No more than 15 characters
    */
  if (username.length > 15) {
    return 0;
  }
  const res = /^[a-z0-9_\.]+$/.exec(username);
  const valid = !!res;
  return valid;
}

export async function isAdmin(login: string): Promise<boolean> {
  if (process.env.FT_ADMINS == undefined || login == undefined) {
    return false;
  }
  const listeAdmin = await process.env.FT_ADMINS.split(",");
  return listeAdmin.includes(login);
}

export function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function arrayRemove(array, value) {
  return array.filter(function (ele) {
    return ele != value;
  });
}

export function makeCode(length) {
  var result = '';
  var characters = '0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() *
      charactersLength));
  }
  return result;
}

export async function hashPassword(password: string) {
  const bcrypt = require('bcrypt');
  const salt = await bcrypt.genSalt(6);
  return await bcrypt.hash(password, salt);
}

export async function comparePassword(passwordhash: string, password: string) {
  const bcrypt = require('bcrypt');
  return await bcrypt.compare(password, passwordhash);
}