import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Friend, FriendState } from '../friends/entities/friend.entity';
import { User } from '../users/entities/user.entity';
import { addErrorToResponseObj, addInternalServerErrorToResponseObj, ResponseOBJ } from '../utils-dto/utils.dto';
import { Like, Repository } from 'typeorm';

@Injectable()
export class ProfilesService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) { }

    //obtenir le profil public d'un compte
    async getProfileInfo(responseObj: ResponseOBJ, username: string, query): Promise<string> {
        const user = await User.findOne({
            where: [
                { 'username': username as string },
            ],
            select: [
                'username',
                'avatar',
                'wins',
                'loses',
                'ratio',
                'xp',
                'status',
                'lvl',
                'playingCurrently',
                'createdAt'
            ],

        })

        if (user == undefined) {
            addErrorToResponseObj(responseObj, "requested profile does not exist.", 0);
        } else {
            responseObj.data = user

            let user_first_login = ''
            let user_second_login = ''

            if (query['login'] < username) {
                user_first_login = query['login']
                user_second_login = username
            } else {
                user_first_login = username
                user_second_login = query['login']
            }

            const count = await Friend.count({
                where: [
                    {
                        'user_first_login': user_first_login,
                        'user_second_login': user_second_login,
                        'state': FriendState.BLOCK_BY_FIRST,
                    },

                    {
                        'user_first_login': user_first_login,
                        'user_second_login': user_second_login,
                        'state': FriendState.BLOCK_BY_SECOND,
                    },
                ]
            })

            if (count === 0) {
                responseObj.data['isblocked'] = false
            } else {
                responseObj.data['isblocked'] = true
            }


            const count2 = await Friend.count({
                where: [
                    {
                        'user_first_login': user_first_login,
                        'user_second_login': user_second_login,
                        'state': FriendState.FRIEND,
                    },
                ]
            })

            if (count2 === 0) {
                responseObj.data['isfriend'] = false
            } else {
                responseObj.data['isfriend'] = true
            }

        }
        return JSON.stringify(responseObj);
    }

    //rechercher un profil
    async searchProfile(responseObj: ResponseOBJ, query): Promise<string> {
        let username = query['username'] as string
        let liste = []

        let usersListe = await User.find({
            where: [
                { username: Like(`%${username}%`) }
            ],
            take: 10,
        })

        usersListe.forEach(element => {
            if (element.login !== query['login']) {
                liste.push({
                    'login': element.login,
                    'username': element.username,
                    'avatar': element.avatar,
                    'wins': element.wins,
                    'loses': element.loses,
                    'ratio': element.ratio,
                    'xp': element.xp,
                    'status': element.status,
                    'lvl': element.lvl,
                    'playingCurrently': element.playingCurrently,
                    'created_at': element.createdAt
                })
            }
        })
        responseObj.data = liste
        return JSON.stringify(responseObj);
    }

    //obtenir la liste de profils bloqués
    async getListOfProfileBlocked(responseObj: ResponseOBJ, query): Promise<string> {
        let liste = []
        if (query['index'] as number < 1) {
            query['index'] = 1;
        }
        await Friend.find({
            relations: ["users"],
            where: [
                { user_first_login: query['login'], state: FriendState.BLOCK_BY_FIRST },
                { user_second_login: query['login'], state: FriendState.BLOCK_BY_SECOND },
            ],
            order: {
                user_first_login: "ASC",
                user_second_login: "ASC"
            },
            skip: query['index'] as number === 0 ? 0 : ((10 * query['index'] as number) - 10),
            take: 10,
        }
        ).then(res => {
            res.forEach(element => {
                element.users.forEach(user => {
                    if (user.login != query['login']) {
                        liste.push({
                            'username': user.username,
                            'avatar': user.avatar,
                            'created_at': element.created_at, id: element.id
                        })
                    }
                })
            })
            responseObj.data = liste;
        })
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }

    //bloquer un profil
    async blockProfile(responseObj: ResponseOBJ, query): Promise<string> {

        const myAccount = await User.findOne({
            where: [
                { 'login': query['login'] },
            ]
        })

        if (myAccount.username === query['username'] as string) {
            addErrorToResponseObj(responseObj, "you can't block yourself", 0);
            return JSON.stringify(responseObj)
        }

        const userContact = await User.findOne({
            where: [
                { 'username': query['username'] },
            ]
        })

        if (userContact === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to block/unblock is unknown", 1);
            return JSON.stringify(responseObj)
        }

        let user_first_login = ''
        let user_second_login = ''
        let state_request;

        if (myAccount.login < userContact.login) {
            user_first_login = myAccount.login
            user_second_login = userContact.login
            state_request = FriendState.BLOCK_BY_FIRST;
        } else {
            user_first_login = userContact.login
            user_second_login = myAccount.login
            state_request = FriendState.BLOCK_BY_SECOND;
        }

        let count = await Friend.count({
            where: [
                {
                    'user_first_login': user_first_login,
                    'user_second_login': user_second_login
                },
            ]
        })

        if (count === 0) {
            const friend = new Friend()
            friend.user_first_login = user_first_login
            friend.user_second_login = user_second_login
            friend.users = [myAccount, userContact]
            friend.state = state_request
            await friend.save()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                    addInternalServerErrorToResponseObj(responseObj);
                });
        } else {
            await Friend.createQueryBuilder()
                .update()
                .set({ state: state_request })
                .where('user_first_login=:user_first_login AND user_second_login=:user_second_login', {
                    'user_first_login': user_first_login,
                    'user_second_login': user_second_login
                })
                .execute()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                    addInternalServerErrorToResponseObj(responseObj);
                });
        }
        return JSON.stringify(responseObj);
    }

    //débloquer un profil
    async unblockProfile(responseObj: ResponseOBJ, query): Promise<string> {
        const userContact = await User.findOne({
            where: [
                { 'username': query['username'] },
            ]
        })
        if (userContact === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to block/unblock is unknown", 0);
            return JSON.stringify(responseObj)
        }


        let user_first_login = ''
        let user_second_login = ''
        let loginPosition = 1

        if (query['login'] < userContact.login) {
            user_first_login = query['login']
            user_second_login = userContact.login
            loginPosition = 1
        } else {
            user_first_login = userContact.login
            user_second_login = query['login']
            loginPosition = 2
        }


        if (loginPosition === 1) {

            await Friend.createQueryBuilder()
                .delete()
                .where('user_first_login =:user_first_login AND  user_second_login =:user_second_login AND state =:state', {
                    user_first_login: query['login'],
                    user_second_login: userContact.login,
                    state: FriendState.BLOCK_BY_FIRST
                })
                .execute()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                    addInternalServerErrorToResponseObj(responseObj);
                });
        } else if (loginPosition === 2) {
            await Friend.createQueryBuilder()
                .delete()
                .where('user_first_login =:user_first_login AND  user_second_login =:user_second_login AND state =:state', {
                    user_first_login: userContact.login,
                    user_second_login: query['login'],
                    state: FriendState.BLOCK_BY_SECOND
                })
                .execute()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                    addInternalServerErrorToResponseObj(responseObj);
                });
        }
        return JSON.stringify(responseObj);
    }
}
