import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { ProfilesController } from './profiles.controller';
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User]), CacheModule.register({ ttl: 0 })],
  controllers: [ProfilesController],
  providers: [ProfilesService]
})
export class ProfilesModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(ProfilesController);
  }
}
