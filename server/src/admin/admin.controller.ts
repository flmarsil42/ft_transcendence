import { Controller, Get, Put, Post, Delete, Header, Res, Query } from '@nestjs/common';
import { AdminService } from './admin.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('api/v1')
export class AdminController {
    constructor(private readonly adminService: AdminService) { }

    @Get('/admin/stats')
    async getAppStat(@Query() query): Promise<string> {
        return await this.adminService.getAppStat(initResponse(query), query);
    }

    @Get('/admin/searchprofiles')
    async getProfilesListe(@Query() query): Promise<string> {
        return await this.adminService.getProfilesListe(initResponse(query), query);
    }

    @Put('/admin/resetprofile')
    async resetProfile(@Query() query): Promise<string> {
        return await this.adminService.resetProfile(initResponse(query), query);
    }

    @Get('/admin/profile')
    async getProfile(@Query() query): Promise<string> {
        return await this.adminService.getProfile(initResponse(query), query);
    }

    @Get('/admin/bans')
    async getBanListe(@Query() query): Promise<string> {
        return await this.adminService.getBanListe(initResponse(query), query);
    }

    @Post('/admin/ban')
    async setBan(@Query() query): Promise<string> {
        return await this.adminService.setBan(initResponse(query), query);
    }

    @Delete('/admin/ban')
    async unBan(@Query() query): Promise<string> {
        return await this.adminService.unBan(initResponse(query), query);
    }

}