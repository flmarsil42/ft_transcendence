import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { isAdmin, addErrorToResponseObj, addInternalServerErrorToResponseObj, ResponseOBJ, hashPassword, comparePassword } from '../utils-dto/utils.dto';
import { Brackets, getConnection, Like, QueryBuilder, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { Ban } from './entities/ban.entity';
import { AppStats } from './entities/appStats.entity';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<[User, Ban, AppStats]>,
    ) {
        this.init()
    }

    async init() {
        await getConnection().query('TRUNCATE doubleauth');
        await getConnection().query('TRUNCATE games');
        await getConnection().query('TRUNCATE challenges');
        // await getConnection().query('TRUNCATE usersessions');
        const count = await AppStats.count({})
        if (count == 0) {
            const stast = new AppStats();
            stast.save();
            //console.log(" ---------------- creation de stats")
        } else {
            //console.log(" ---------------- stats exist")
        }

        const count2 = await User.count({
            where: [
                { 'login': "CompteTestLogin" },
            ],
        })

        const user = new User()
        user.login = "CompteTestLogin"
        user.username = "CompteTestUsername"
        user.avatar = "https://cdn.intra.42.fr/users/medium_salty.jpg"
        user.email = "CompteTestMail@42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();

        if (process.env.FT_AUTHENTICATION === String("false")) {
            await this.createTestAccounts()
            //console.log("compte test")
        }


        await User.createQueryBuilder()
            .update()
            .set({ playingCurrently: false })
            .execute()
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
            });

        /*let hash = await hashPassword("fouad") as string
         //console.log("paswword :", hash)
         //console.log("result", await comparePassword(hash, "fouad"))*/

    }

    async createTestAccounts() {
        const user = new User()
        user.login = "fassani"
        user.username = "fassani"
        user.avatar = "https://cdn.intra.42.fr/users/medium_fassani.jpg"
        user.email = "testmail0@student.42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();

        user.login = "flmarsil"
        user.username = "flmarsil"
        user.avatar = "https://cdn.intra.42.fr/users/medium_flmarsil.jpg"
        user.email = "testmail1@student.42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();

        user.login = "charmstr"
        user.username = "charmstr"
        user.avatar = "https://cdn.intra.42.fr/users/medium_charmstr.jpg"
        user.email = "testmail2@student.42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();


        user.login = "melissa2"
        user.username = "melissa2"
        user.avatar = "https://cdn.intra.42.fr/users/medium_melissa.png"
        user.email = "testmail3@student.42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();

        user.login = "fabienne2"
        user.username = "fabienne2"
        user.avatar = "https://cdn.intra.42.fr/users/medium_fabienne.jpg"
        user.email = "testmail4@student.42.fr"

        await User.createQueryBuilder()
            .insert()
            .into(User)
            .values(user)
            .orIgnore()
            .execute();

    }

    async getAppStat(responseObj: ResponseOBJ, query): Promise<string> {
        const stats = await AppStats.findOne({})
        responseObj.data = stats
        return JSON.stringify(responseObj);
    }

    async getProfilesListe(responseObj: ResponseOBJ, query): Promise<string> {

        let username = query['username'] as string

        let usersListe = await User.find({
            where: [
                { username: Like(`%${username}%`) }
            ],
            select: [
                'id',
                'login',
                'username',
                'avatar',
                'createdAt'
            ],
            take: 10,
        })
        responseObj.data = usersListe
        return JSON.stringify(responseObj);
    }

    async resetProfile(responseObj: ResponseOBJ, query): Promise<string> {
        const user = await User.findOne({
            where: [
                { 'username': query['username'] as string },
            ]
        })

        if (user == undefined) {
            addErrorToResponseObj(responseObj, "requested user does not exist", 0);
        } else {
            user.wins = 0
            user.loses = 0
            user.ratio = '0'
            user.xp = 0
            user.lvl = 0
            await user.save()
        }
        return JSON.stringify(responseObj);
    }

    async getProfile(responseObj: ResponseOBJ, query): Promise<string> {
        const user = await User.findOne({
            where: [
                { 'username': query['username'] as string },
            ]
        })

        if (user == undefined) {
            addErrorToResponseObj(responseObj, "requested user does not exist", 0);
        } else {
            responseObj.data = user
        }
        return JSON.stringify(responseObj);
    }

    async getBanListe(responseObj: ResponseOBJ, query): Promise<string> {
        const banListe = await Ban.find({
            order: {
                login: "ASC",
            },
            skip: query['index'] as number === 0 ? 0 : ((10 * query['index'] as number) - 10),
            take: 10,
        })
        responseObj.data = banListe
        return JSON.stringify(responseObj);
    }

    async setBan(responseObj: ResponseOBJ, query): Promise<string> {
        const user = await User.findOne({
            where: [
                { 'username': query['username'] as string },
            ]
        })

        if (user == undefined) {
            addErrorToResponseObj(responseObj, "requested user does not exist", 0);
        } else {

            const userBan = await Ban.findOne({
                where: [
                    { 'login': user.login as string },
                ]
            })

            if (userBan != undefined) {
                userBan.reason = query['reason']
                await userBan.save()
            } else {
                const ban = new Ban();
                ban.login = user.login
                ban.reason = query['reason']
                await ban.save();
            }
        }
        return JSON.stringify(responseObj);
    }

    async unBan(responseObj: ResponseOBJ, query): Promise<string> {

        const user = await User.findOne({
            where: [
                { 'username': query['username'] },
            ]
        })

        if (user === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to unban  is unknown", 0);
            return JSON.stringify(responseObj)
        }
        await Ban.createQueryBuilder()
            .delete()
            .where('login =:login', { login: user.login })
            .execute()
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }
}