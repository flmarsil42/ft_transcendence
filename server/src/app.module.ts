import { Module, CacheModule, Injectable, CACHE_MANAGER, Inject, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { ChatChannelsModule } from './chatChannels/chatChannels.module';
import { ApiModule } from './api/api.module';
import { SdkModule } from './sdk/sdk.module';
import { FriendsModule } from './friends/friends.module';
import { GamesModule } from './games/games.module';
import { ProfilesModule } from './profiles/profiles.module';
import { UsersModule } from './users/users.module';
import { HttpModule } from '@nestjs/axios';
import { UserSession } from './userSessions/entities/userSession.entity'
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthentificationMiddleware } from './middlewares/authentification.middleware'
import { GameHistoryModule } from './gameHistory/gameHistory.module';
import { ChatGateway } from './chat/chat.gateway';
import { ChatModule } from './chat/chat.module';
import { PongModule } from './pong/pong.module';
import { PongGateway } from './pong/pong.gateway';
import { ConfigModule } from '@nestjs/config';
import { DirectMessagesModule } from './direct-messages/direct-messages.module';
import { AdminModule } from './admin/admin.module';
import { MailModule } from './mail/mail.module';

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([UserSession]), CacheModule.register({ ttl: 0 }), DatabaseModule, ChatChannelsModule, ApiModule, SdkModule, FriendsModule, GamesModule, ProfilesModule, UsersModule, HttpModule, GameHistoryModule, ChatModule, PongModule, DirectMessagesModule, AdminModule, MailModule],
  controllers: [AppController],
  providers: [AppService, PongGateway, ChatGateway],
})

@Injectable()
export class AppModule {
  /*configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .exclude(
        { path: '/', method: RequestMethod.GET },
        { path: '/api/v1/isConnected', method: RequestMethod.GET },
        { path: '/api/v1/login', method: RequestMethod.GET },
        { path: '/api/v1/send2faCode', method: RequestMethod.PUT },
        '/(.*)',
      )
      .forRoutes(AppController);
  }*/
}
