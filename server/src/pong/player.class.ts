import { DIRECTION } from "./pong_utils.dto";
import { Paddle } from "./paddle.class";

export class Player {
    private username_: string;
    private isReady_: boolean
    private score_: number
    private map_: string
    private key_: DIRECTION
    private paddle_: Paddle
    private powerUp_: string


    constructor(username: string) {
        this.username_ = username
        this.isReady_ = false
        this.score_ = 0
        this.map_ = undefined
        this.key_ = DIRECTION.IDLE
        this.paddle_ = new Paddle(3, 60, 18)
        this.powerUp_ = undefined

    }

    getUsername(): string { return this.username_ }
    getIsReady(): boolean { return this.isReady_ }
    getScore(): number { return this.score_ }
    getMap(): string { return this.map_ }
    getKey(): DIRECTION { return this.key_ }
    getPaddle(): Paddle { return this.paddle_ }
    getPowerUP(): string { return this.powerUp_ }

    toObj(): object {
        return {
            'username': this.getUsername(),
            'powerUP': this.getPowerUP(),
            'score': this.getScore()
        }
    }

    setUsername(username: string): string { return this.username_ = username }
    setIsReady(isReady: boolean): boolean { return this.isReady_ = isReady }
    setScore(score: number): number { return this.score_ = score }
    setMap(map: string): string { return this.map_ = map }
    setKey(key: DIRECTION): DIRECTION { return this.key_ = key }
    setPaddle(paddle: Paddle): Paddle { return this.paddle_ = paddle }
    setPowerUP(powerUp: string): string { return this.powerUp_ = powerUp }

}