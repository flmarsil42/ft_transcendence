import { CACHE_MANAGER, Inject } from "@nestjs/common";
import { Server, Socket } from "socket.io";
import { Cache } from "cache-manager";
import { DIRECTION } from "./pong_utils.dto";
import { User } from "../users/entities/user.entity";
import { sleep } from "../utils-dto/utils.dto";
import { Map } from "./map.class";
import { Ball } from "./ball.class";
import { Player } from "./player.class";
import { GameHistory, GameHistoryGameType } from "../gameHistory/entities/gameHistory.entity"
import { Game } from "../games/entities/game.entity";

let game_calculation_speed = 40

export class GameInstance {
  game_State = {
    gameStarted: false,
    dateStart: undefined,
    dateEnd: undefined as Date,
    map: undefined,
  };

  turn: DIRECTION;
  map: Map;
  ball: Ball;
  player1: Player;
  player2: Player;

  constructor(
    private gameID: string,
    private player1_username: string,
    private player2_username: string,
    private soc_player1: Socket,
    private soc_player2: Socket,
    private server: Server,
    private gameType: GameHistoryGameType,
    private mapName: string
  ) {
    this.turn = DIRECTION.LEFT;
    this.map = new Map(this.mapName);
    this.ball = new Ball(this.map.getSize_ball(), this.map.getSpeed_ball());
    this.player1 = new Player(this.player1_username);
    this.player2 = new Player(this.player2_username);
    this.resetPlayerPosition();
    this.resetBallPosition();
    this.init();
  }

  resetBallPosition() {
    this.ball.setSize(this.map.getSize_ball());
    this.ball.setSpeed(this.map.getSpeed_ball());
    this.ball.setX(this.map.getWidth() / 2 - this.ball.getSize() / 2);
    this.ball.setY(
      Math.floor(Math.random() * (this.map.getHeight() - 200)) + 200
    );
    this.ball.setMoveX(this.turn);
    this.ball.setMoveY(
      [DIRECTION.UP, DIRECTION.DOWN][Math.round(Math.random())]
    );
  }

  resetPlayerPosition() {
    this.resetBallPosition()
    this.player1.getPaddle().setWidth(this.map.getPaddle_width());
    this.player1.getPaddle().setHeight(this.map.getPaddle_height());
    this.player1.getPaddle().setX(this.map.getPaddle_marging());
    this.player1.getPaddle().setY(this.map.getHeight() / 2 - this.map.getPaddle_height() / 2);
    this.player1.getPaddle().setSpeed(this.map.getPlayer_speed());
    this.player1.setKey(DIRECTION.IDLE);

    this.player2.getPaddle().setWidth(this.map.getPaddle_width());
    this.player2.getPaddle().setHeight(this.map.getPaddle_height());
    this.player2.getPaddle().setX(this.map.getPaddle_marging());
    this.player2.getPaddle().setX(this.map.getWidth() - this.map.getPaddle_marging());
    this.player2.getPaddle().setY(this.map.getHeight() / 2 - this.map.getPaddle_height() / 2);
    this.player2.getPaddle().setSpeed(this.map.getPlayer_speed());
    this.player2.setKey(DIRECTION.IDLE);
  }

  async newRound() {
    await this.saveScores(this.gameID)

    if (this.turn === DIRECTION.LEFT) {
      this.turn = DIRECTION.RIGHT;
    } else {
      this.turn = DIRECTION.LEFT;
    }

    this.map.setGamePoints(this.map.getGamePoints() - 1)
    let update = {
      gameId: this.gameID,
      time: undefined,
      player1: this.player1.toObj(),
      player2: this.player2.toObj(),
      nbBall: this.map.getGamePoints(),
    };


    this.resetPlayerPosition();
    await this.server.to(this.gameID).volatile.compress(true).emit("gameFrame", {
      data: {
        player1: this.player1.getPaddle().toObj(),
        player2: this.player2.getPaddle().toObj(),
        ball: this.ball.toObj(),
      },
    });

    await this.server.to(this.gameID).emit("gameUpdate", { data: update });
    await sleep(2000);
  }

  async init() {
    this.server.to(this.soc_player1.id).socketsJoin(this.gameID);
    this.server.to(this.soc_player2.id).socketsJoin(this.gameID);

    this.soc_player1.on("disconnect", (reason) => {
      this.game_State.gameStarted = false;
      this.player1.setScore(-42);
      //console.log(this.player1.getUsername(), "leave the game");
      this.server.to(this.gameID).emit("playerLeave", { data: this.player1.getUsername() });
    });

    this.soc_player2.on("disconnect", (reason) => {
      this.game_State.gameStarted = false;
      this.player2.setScore(-42);
      //console.log(this.player2.getUsername(), "leave the game");
      this.server.to(this.gameID).emit("playerLeave", { data: this.player2.getUsername() });
    });

    this.soc_player1.on("ready", (...args) => {
      this.player1.setIsReady(true);
      this.server.to(this.gameID).emit("playerReady", { data: this.player1.getUsername() });
      if (this.player2.getIsReady()) {
        this.startGame();
      }
    });

    this.soc_player2.on("ready", (...args) => {
      this.player2.setIsReady(true);
      this.server.to(this.gameID).emit("playerReady", { data: this.player2.getUsername() });
      if (this.player1.getIsReady()) {
        this.startGame();
      }
    });

    this.soc_player1.on("leave", (...args) => {
      this.game_State.gameStarted = false;
      this.player1.setScore(-42);
      //console.log(this.player1.getUsername(), "leave the game");
      this.server.to(this.gameID).emit("playerLeave", { data: this.player1.getUsername() });
    });

    this.soc_player2.on("leave", (...args) => {
      this.game_State.gameStarted = false;
      this.player2.setScore(-42);
      //console.log(this.player2.getUsername(), "leave the game");
      this.server.to(this.gameID).emit("playerLeave", { data: this.player2.getUsername() });
    });

    this.server.to(this.gameID).emit("gameFound", {
      data: {
        gameId: this.gameID,
        player1: this.player1.getUsername(),
        player2: this.player2.getUsername(),
        scorePlayer1: 0,
        scorePlayer2: 0,
        mapName: this.mapName,
      },
    });


    const User_player1 = await User.findOne({
      where: { username: this.player1_username },
    });

    const User_player2 = await User.findOne({
      where: { username: this.player2_username },
    });

    User_player1.playingCurrently = true
    User_player2.playingCurrently = true

    await User_player1.save()
    await User_player2.save()

  }

  positionRaquette() {
    // if (ord_raquette2 >= h) { ord_raquette2 = ord_raquette2 - deplace };
    //mannque les collision avec les murs

    //fleche bas player1
    if (this.player1.getKey() == DIRECTION.DOWN) {
      this.player1
        .getPaddle()
        .setY(
          this.player1.getPaddle().getY() - this.player1.getPaddle().getSpeed()
        );
    }

    //fleche haut player1
    if (this.player1.getKey() == DIRECTION.UP) {
      this.player1
        .getPaddle()
        .setY(
          this.player1.getPaddle().getY() + this.player1.getPaddle().getSpeed()
        );
    }

    // If the player1 collides with the bound limits, update the x and y coords.
    if (this.player1.getPaddle().getY() <= 0) {
      this.player1.getPaddle().setY(0);
    } else if (
      this.player1.getPaddle().getY() >=
      this.map.getHeight() - this.player1.getPaddle().getHeight()
    ) {
      this.player1
        .getPaddle()
        .setY(this.map.getHeight() - this.player1.getPaddle().getHeight());
    }

    /////////////////////////////////////////////////////

    //fleche bas player2
    if (this.player2.getKey() == DIRECTION.DOWN) {
      this.player2
        .getPaddle()
        .setY(
          this.player2.getPaddle().getY() - this.player2.getPaddle().getSpeed()
        );
    }

    //fleche haut player2
    if (this.player2.getKey() == DIRECTION.UP) {
      this.player2
        .getPaddle()
        .setY(
          this.player2.getPaddle().getY() + this.player2.getPaddle().getSpeed()
        );
    }

    // If the player1 collides with the bound limits, update the x and y coords.
    if (this.player2.getPaddle().getY() <= 0) {
      this.player2.getPaddle().setY(0);
    } else if (
      this.player2.getPaddle().getY() >=
      this.map.getHeight() - this.player2.getPaddle().getHeight()
    ) {
      this.player2
        .getPaddle()
        .setY(this.map.getHeight() - this.player2.getPaddle().getHeight());
    }
  }

  positionBalle() {
    // Move ball in intended direction based on moveY and moveX values

    if (this.ball.getMoveY() === DIRECTION.UP) {
      this.ball.setY(this.ball.getY() - this.ball.getSpeed() / 1.5);
    } else if (this.ball.getMoveY() === DIRECTION.DOWN) {
      this.ball.setY(this.ball.getY() + this.ball.getSpeed() / 1.5);
    }

    if (this.ball.getMoveX() === DIRECTION.LEFT) {
      this.ball.setX(this.ball.getX() - this.ball.getSpeed());
    } else if (this.ball.getMoveX() === DIRECTION.RIGHT) {
      this.ball.setX(this.ball.getX() + this.ball.getSpeed());
    }
  }

  async testBalleCollision() {
    // If the ball collides with the bound limits - correct the x and y coords.
    if (this.ball.getX() <= 0) {
      const rand_ = await Math.floor(Math.random() * 2);
      if (this.player1.getPowerUP() === undefined) {
        this.player1.setPowerUP(this.map.getPowersUp()[rand_])
      }

      this.player2.setScore(this.player2.getScore() + 1);
      await this.newRound();
    }
    if (this.ball.getX() >= this.map.getWidth() - this.ball.getSize()) {
      // player 1 score++
      const rand_ = await Math.floor(Math.random() * 2);
      if (this.player2.getPowerUP() === undefined) {
        this.player2.setPowerUP(this.map.getPowersUp()[rand_])
      }

      this.player1.setScore(this.player1.getScore() + 1);
      await this.newRound();
    }
    if (this.ball.getY() <= 0) {
      this.ball.setMoveY(DIRECTION.DOWN);
    }
    if (this.ball.getY() >= this.map.getHeight() - this.ball.getSize()) {
      this.ball.setMoveY(DIRECTION.UP);
    }
  }

  testPlayersCollision() {
    // Handle Player1-Ball collisions
    if (
      this.ball.getX() - this.ball.getSize() <=
      this.player1.getPaddle().getX() &&
      this.ball.getX() >=
      this.player1.getPaddle().getX() - this.player1.getPaddle().getWidth()
    ) {
      if (
        this.ball.getY() <=
        this.player1.getPaddle().getY() +
        this.player1.getPaddle().getHeight() &&
        this.ball.getY() + this.ball.getSize() >=
        this.player1.getPaddle().getY()
      ) {
        this.ball.setX(this.player1.getPaddle().getX() + this.ball.getSize());
        this.ball.setMoveX(DIRECTION.RIGHT);
        this.ball.setSpeed(this.ball.getSpeed() + 2)
      }
    }

    // Handle Player2-Ball collisions
    if (
      this.ball.getX() - this.ball.getSize() <=
      this.player2.getPaddle().getX() &&
      this.ball.getX() >=
      this.player2.getPaddle().getX() - this.player2.getPaddle().getWidth()
    ) {
      if (
        this.ball.getY() <=
        this.player2.getPaddle().getY() +
        this.player2.getPaddle().getHeight() &&
        this.ball.getY() + this.ball.getSize() >=
        this.player2.getPaddle().getY()
      ) {
        this.ball.setX(this.player2.getPaddle().getX() - this.ball.getSize());
        this.ball.setMoveX(DIRECTION.LEFT);
        this.ball.setSpeed(this.ball.getSpeed() + 2)
      }
    }
  }

  async execPowerUP(player: DIRECTION, powerUP: string) {

    if (powerUP === 'SpeedyGonzales') {
      this.ball.effect_SpeedyGonzales()
    } else if (powerUP === 'ReverseDirection') {
      if (player === DIRECTION.LEFT) {
        this.player2.getPaddle().effect_ReverseDirection()
      } else {
        this.player1.getPaddle().effect_ReverseDirection()
      }
    } else if (powerUP === 'ReturnToTheSender') {
      this.ball.effect_ReturnToTheSender()
    } else if (powerUP === 'SuperSizeMe') {
      if (player === DIRECTION.LEFT) {
        this.player1.getPaddle().effect_SuperSizeMe()
      } else {
        this.player2.getPaddle().effect_SuperSizeMe()
      }
    } else if (powerUP === 'HazardousDirection') {
      this.ball.effect_HazardousDirection()
    } else if (powerUP === 'BulletTime') {
      this.ball.effect_BulletTime()
    } else if (powerUP === 'AntMan') {
      if (player === DIRECTION.LEFT) {
        this.player2.getPaddle().effect_AntMan()
      } else {
        this.player1.getPaddle().effect_AntMan()
      }
    }

  }

  async UsePowerUP(player: DIRECTION) {

    if (player === DIRECTION.LEFT) {
      this.execPowerUP(player, this.player1.getPowerUP())
      this.player1.setPowerUP(undefined)
    } else if (player === DIRECTION.RIGHT) {
      this.execPowerUP(player, this.player2.getPowerUP())
      this.player2.setPowerUP(undefined)
    }

  }

  async startGame() {
    const gameEntity = new Game()

    gameEntity.gameID = this.gameID
    gameEntity.username_player1 = this.player1_username
    gameEntity.username_player2 = this.player2_username
    gameEntity.mapName = this.map.getName()

    await gameEntity.save()

    this.game_State.gameStarted = true;

    this.server.to(this.gameID).emit("gameStart");

    this.soc_player1.on("keyRELEASE", (...args) => {
      this.player1.setKey(DIRECTION.IDLE);
    });

    this.soc_player2.on("keyRELEASE", (...args) => {
      this.player2.setKey(DIRECTION.IDLE);
    });

    this.soc_player1.on("keyUP", (...args) => {
      if (this.player1.getPaddle().getReverseDirection()) {
        this.player1.setKey(DIRECTION.UP);
      } else {
        this.player1.setKey(DIRECTION.DOWN);
      }
    });

    this.soc_player2.on("keyUP", (...args) => {
      if (this.player2.getPaddle().getReverseDirection()) {
        this.player2.setKey(DIRECTION.UP);
      } else {
        this.player2.setKey(DIRECTION.DOWN);
      }
    });

    this.soc_player1.on("keyDOWN", (...args) => {
      if (this.player1.getPaddle().getReverseDirection()) {
        this.player1.setKey(DIRECTION.DOWN);
      } else {
        this.player1.setKey(DIRECTION.UP);
      }
    });

    this.soc_player2.on("keyDOWN", (...args) => {
      if (this.player2.getPaddle().getReverseDirection()) {
        this.player2.setKey(DIRECTION.DOWN);
      } else {
        this.player2.setKey(DIRECTION.UP);
      }
    });


    if (this.map.getName() != "classic") {
      this.soc_player1.on("keyPOWERUP", (...args) => {
        this.UsePowerUP(DIRECTION.LEFT)
      });

      this.soc_player2.on("keyPOWERUP", (...args) => {
        this.UsePowerUP(DIRECTION.RIGHT)
      });
    }

    this.game_State.dateStart = new Date()

    while (this.map.getGamePoints() && this.game_State.gameStarted == true) {
      await sleep(game_calculation_speed);
      await this.testBalleCollision();
      this.positionRaquette();
      this.positionBalle();
      this.testPlayersCollision();
      this.server.to(this.gameID).volatile.compress(true).emit("gameFrame", {
        data: {
          player1: this.player1.getPaddle().toObj(),
          player2: this.player2.getPaddle().toObj(),
          ball: this.ball.toObj(),
        },
      });
    }
    await gameEntity.remove()
    this.game_State.dateEnd = new Date()

    let winner;
    let loser;

    if (this.player1.getScore() > this.player2.getScore()) {
      winner = this.player1.toObj()
      loser = this.player2.toObj()
    } else {
      winner = this.player2.toObj()
      loser = this.player1.toObj();
    }

    let result = {
      gameId: this.gameID,
      winner: winner,
      loser: loser,
      time: new Date(),
    };

    let gameHistory = new GameHistory()

    const User_winner = await User.findOne({
      where: { username: result.winner.username },
    });

    const User_loser = await User.findOne({
      where: { username: result.loser.username },
    });

    gameHistory.winnerLogin = User_winner.login
    gameHistory.scoreWinner = result.winner.score

    gameHistory.loserLogin = User_loser.login
    gameHistory.scoreLoser = result.loser.score

    gameHistory.gameDurationInSeconds = Math.round(Math.abs((this.game_State.dateEnd.getTime() - this.game_State.dateStart.getTime()) / 1000));

    gameHistory.gameType = this.gameType
    gameHistory.gameMap = this.map.getName()

    gameHistory.winner = User_winner
    gameHistory.loser = User_loser

    await gameHistory.save()

    let userLeave = false

    if (loser.score == -42) {
      userLeave = true
    }

    this.server.to(this.gameID).emit("gameFinish", { data: result });
    this.server.sockets.socketsLeave(this.gameID);
    await this.user_inc_stat(User_winner, User_loser, userLeave)
  }

  async saveScores(gameId: string) {
    const game = await Game.findOne({
      where: [
        { 'gameID': gameId },
      ],
    })

    game.score_player1 = this.player1.getScore()
    game.score_player2 = this.player2.getScore()
    await game.save()
  }

  async user_inc_stat(User_winner: User, User_loser: User, userLeave: boolean) {

    User_winner.wins = User_winner.wins + 1
    User_loser.loses = User_loser.loses + 1

    User_winner.ratio = (User_winner.loses == 0) ? User_winner.wins.toString() : (User_winner.wins / User_winner.loses).toPrecision(2)
    User_loser.ratio = (User_loser.loses == 0) ? User_loser.wins.toString() : (User_loser.wins / User_loser.loses).toPrecision(2)

    if ((User_winner.xp + 10) > 100) {
      let xp = ((User_winner.xp + 10) - 100)
      User_winner.xp = xp
      User_winner.lvl = User_winner.lvl + 1
    } else {
      User_winner.xp = (User_winner.xp + 10)
    }

    if (userLeave === true) {
      if ((User_loser.xp - 42) < 0) {
        let xp = ((User_loser.xp - 42) + 100)
        if (User_loser.lvl === 0) {
          User_loser.xp = 0
          User_loser.lvl = 0
        } else {
          User_loser.xp = xp
          User_loser.lvl = User_loser.lvl - 1
        }

      } else {
        User_loser.xp = (User_loser.xp - 42)
      }
    }

    User_winner.playingCurrently = false
    User_loser.playingCurrently = false

    await User_winner.save()
    await User_loser.save()
  }
}
