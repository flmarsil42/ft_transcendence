
export function getIntraApiLocation(): string {
    let crypto = require("crypto");
    let state = crypto.randomBytes(30).toString('hex');
    let intra_location = process.env.API42_HOST + '/oauth/authorize?client_id=' + process.env.API42_CLIENT_ID + '&redirect_uri=' + process.env.FT_LOGIN + '&response_type=code&scope=public&state=' + state
    return intra_location;
}
