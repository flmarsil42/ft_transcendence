import { Controller, Query, Get, Post, Put, Body, Patch, Param, Delete } from '@nestjs/common';
import { ChatChannelsService } from './chatChannels.service';
import { initResponse } from '../utils-dto/utils.dto';
import { ChatChannelUserState } from './entities/chatChannelBannedOrMutedUser.entity';

@Controller('api/v1')
export class ChatChannelsController {
  constructor(private readonly chatChannelsService: ChatChannelsService) { }

  @Post('/chatChannel')
  async createOneChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.createOneChatChannel(initResponse(query), query);
  }

  @Delete('/chatChannel')
  async deleteOneChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.deleteOneChatChannel(initResponse(query), query);
  }

  @Post('/chatChannel/membership')
  async joinPublicChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.joinPublicChatChannel(initResponse(query), query);
  }

  @Delete('/chatChannel/membership')
  async quitChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.quitChatChannel(initResponse(query), query);
  }

  @Post('/chatChannel/invite')
  async inviteMemberToPrivateChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.inviteMemberToPrivateChatChannel(initResponse(query), query);
  }

  @Get('/chatChannels')
  async getListOfPublicChatChannels(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfPublicChatChannels(initResponse(query), query);
  }

  @Get('/chatChannels/asOwner')
  async getListOfChatChannelsAsOwner(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfChatChannelsAsOwner(initResponse(query), query);
  }

  @Get('/chatChannels/asAnyMember')
  async getListOfChatChannelsAsAnyMember(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfChatChannelsAsAnyMember(initResponse(query), query);
  }

  @Get('/chatChannel/profiles')
  async getListOfAllProfilesInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfAllProfilesInChatChannel(initResponse(query), query);
  }

  @Get('/chatChannel/adminProfiles')
  async getListOfAdminProfilesInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfAdminProfilesInChatChannel(initResponse(query), query);
  }

  @Get('/chatChannel/guestProfiles')
  async getListOfGuestProfilesInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfGuestProfilesInChatChannel(initResponse(query), query);
  }

  @Post('/chatChannel/password')
  async changePasswordInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.changePasswordInChatChannel(initResponse(query), query);
  }

  @Post('/chatChannel/admin')
  async adminAddToChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.adminAddToChatChannel(initResponse(query), query);
  }

  @Delete('/chatChannel/admin')
  async adminRemoveFromChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.adminRemoveFromChatChannel(initResponse(query), query);
  }

  @Post('/chatChannel/ban')
  async banUserInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.chatChannelBanOrMuteUserInChannel(initResponse(query), query, ChatChannelUserState.BANNED);
  }

  @Delete('/chatChannel/ban')
  async banUserInChatChannelUndo(@Query() query): Promise<string> {
    return await this.chatChannelsService.chatChannelUndoBanOrMuteOnUserInChannel(initResponse(query), query, ChatChannelUserState.BANNED);
  }

  @Get('/chatChannel/ban')
  async getListOfBannedUsersInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfBannedUserInChannel(initResponse(query), query, ChatChannelUserState.BANNED);
  }


  @Post('chatChannel/mute')
  async muteUserInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.chatChannelBanOrMuteUserInChannel(initResponse(query), query, ChatChannelUserState.MUTED);
  }

  @Delete('chatChannel/mute')
  async muteUserInChatChannelUndo(@Query() query): Promise<string> {
    return await this.chatChannelsService.chatChannelUndoBanOrMuteOnUserInChannel(initResponse(query), query, ChatChannelUserState.MUTED);
  }

  @Get('/chatChannel/mute')
  async getListOfMutedUsersInChatChannel(@Query() query): Promise<string> {
    return await this.chatChannelsService.getListOfBannedUserInChannel(initResponse(query), query, ChatChannelUserState.MUTED);
  }

}
