import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { ChatChannelsService } from './chatChannels.service';
import { ChatChannelsController } from './chatChannels.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatChannel } from './entities/chatChannel.entity';
import { UsersModule } from '../users/users.module';
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'
import { AdminAuthentificationMiddleware } from '../middlewares/adminAuthentification.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([ChatChannel]), UsersModule, CacheModule.register({ ttl: 0 })],
  exports: [TypeOrmModule],
  controllers: [ChatChannelsController],
  providers: [ChatChannelsService]
})
export class ChatChannelsModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(ChatChannelsController);
  }
}
