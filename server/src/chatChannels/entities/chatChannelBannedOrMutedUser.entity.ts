import { Entity, OneToMany, JoinTable, JoinColumn, CreateDateColumn, ManyToOne, ManyToMany, PrimaryGeneratedColumn, Column, BaseEntity, OneToOne, ChildEntity, UpdateDateColumn, } from 'typeorm';
import { User } from "../../users/entities/user.entity"
import { ChatChannel } from './chatChannel.entity';

export enum ChatChannelUserState {
    BANNED = 'BANNED',
    MUTED = 'MUTED'
}

@Entity({ name: "chatChannelBannedOrMutedUsers" })
export class ChatChannelBannedOrMutedUser extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne((type) => ChatChannel, (chatChannel) => chatChannel.chatChannelBannedOrMutedUsers, {
        cascade: true, //do something in the spirit of: this.chatChannel = chatChnnel; then do: save(this);
        onDelete: 'CASCADE', //if we delete a chatChannel, the rows related in this table are deleted!
    })
    @JoinColumn()
    chatChannel!: ChatChannel;

    @Column()
    userLogin: string; //even if the user is deleted, we still keep a record of his login!

    @ManyToOne((type) => User, (user) => user.chatChannelBannedOrMutedUsers,
        {
            nullable: true,
            onDelete: 'SET NULL'
        })
    user: User;

    @Column({ default: 'MUTED' })
    state: ChatChannelUserState;

    @UpdateDateColumn() //need UpdatedDate and not CreatedDate since a row cat shift from MUTED to BANNED.
    updateddAt: Date

    @Column()
    expiryDate: Date
}