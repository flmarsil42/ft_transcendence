import { CACHE_MANAGER, Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, query, NextFunction } from 'express';
import { Cache } from 'cache-manager';
import { isAdmin, initResponse, ResponseStatus, addErrorToResponseObj, addInternalServerErrorToResponseObj } from '../utils-dto/utils.dto';
import { getIntraApiLocation } from '../apiConfig'
import { connected } from 'process';
import { stringify } from 'querystring';

@Injectable()
export class AdminAuthentificationMiddleware implements NestMiddleware {
    async use(req: Request, res: Response, next: NextFunction) {
        const login = req.query["login"] as string

        if (await isAdmin(login) == true) {
            next();
            return
        } else {
            let responseOBj = initResponse({})
            addErrorToResponseObj(responseOBj, "Unauthorized.", 20)
            res.send(JSON.stringify(responseOBj))
            return
        }
    }
}