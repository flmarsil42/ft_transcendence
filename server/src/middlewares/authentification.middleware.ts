import { CACHE_MANAGER, Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, query, NextFunction } from 'express';
import { Cache } from 'cache-manager';
import { initResponse, ResponseStatus, addErrorToResponseObj, addInternalServerErrorToResponseObj } from '../utils-dto/utils.dto';
import { getIntraApiLocation } from '../apiConfig'
import { connected } from 'process';
import { stringify } from 'querystring';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Ban } from '../admin/entities/ban.entity';

@Injectable()
export class AuthentificationMiddleware implements NestMiddleware {

    constructor(
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {

    }
    async use(req: Request, res: Response, next: NextFunction) {
        //console.log("\x1b[41m%s\x1b[0m", 'Authentification Middleware Function...');
        //console.log("-------- method:", req.method, "request:", req.path, "Login:", "[" + req.query["login"] + "]");

        if (process.env.FT_AUTHENTICATION === String("false")) {

            //console.log("\x1b[45m%s\x1b[0m", "Warning authentication disabled !!!!!!!!!!!!");
            try {
                const cookieOBJ = JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
                req.query['login'] = cookieOBJ.login;
                // //console.log("setiing login to const string charmstr"); req.query['login'] = 'charmstr';
                //console.log(req.query['login']);
            } catch {
                //console.log("cookieOBJ undefined - a");
            }
            //console.log("\n\x1b[41m%s\x1b[0m", '.... End Authentification Middleware Function');
            next();
            return
        }

        let responseOBj = initResponse({})
        let cacheOBJ;
        let cookieOBJ;

        let loginPage = process.env.FT_HOMEPAGE

        try {
            cookieOBJ = JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
        } catch {
            if (req.cookies['ft_transcendence_cookie'] == undefined) {
                addErrorToResponseObj(responseOBj, "Cookie undefined, Make sure you sent the authentification cookie", 10)
            }

            res.statusCode = 302
            res.set({ 'Location': loginPage });
            res.send(JSON.stringify(responseOBj))
            //console.log('Error: cookie undefined, the client has not sent a cookie')
            //console.log("\x1b[41m%s\x1b[0m", '.... End Authentification Middleware Function');
            return
        }

        if ((cookieOBJ['login'] === undefined) || (cookieOBJ['token'] === undefined)) {

            addErrorToResponseObj(responseOBj, "Invalid cookie or corrupted cookie", 11)

            res.statusCode = 520
            res.set({ 'Location': loginPage });
            res.send(JSON.stringify(responseOBj))
            //console.log('Error: invalid cookie')
            //console.log("\x1b[41m%s\x1b[0m", '.... End Authentification Middleware Function');
            return
        }
        //console.log('user login :', cookieOBJ['login'])

        try {
            const cache = await this.cacheManager.get(cookieOBJ['login'])
            if (cache == undefined) {
                throw ("Error: cache undefined")
            }
            if (typeof (cache) === 'string') {// je ne sais pas pourquoi, mais parfois cache est de type string ...
                cacheOBJ = JSON.parse(cache)
            } else {
                cacheOBJ = cache
            }
        } catch (err) {
            addErrorToResponseObj(responseOBj, "Internal Server Error, make sure you are well connected", 12)
            res.statusCode = 302
            res.set({ 'Location': loginPage });
            res.send(JSON.stringify(responseOBj))
            //console.log('Error: cache undefined, user not find on cache memory, make sure it is properly connected')
            //console.log("\x1b[41m%s\x1b[0m", '.... End Authentification Middleware Function');
            return
        }

        //console.log("---------- cache :", cacheOBJ)
        //console.log("---------- cookie :", cookieOBJ)


        if (cookieOBJ['token'] != cacheOBJ['token']) { /// check si le cookie token est valide 
            addErrorToResponseObj(responseOBj, "Invalid cookie token, make sure you are well connected.", 13)
            res.statusCode = 302
            res.set({ 'Location': loginPage });
            res.send(JSON.stringify(responseOBj))
            //console.log('Error: invalid cookie token')
            //console.log("\x1b[41m%s\x1b[0m", '.... End Middleware Function');
            return
        }

        req.query['login'] = cookieOBJ['login']
        const userBan = await Ban.findOne({
            where: [
                { 'login': cookieOBJ['login'] },
            ]
        })

        if (userBan != undefined) {
            responseOBj.login = cookieOBJ['login']
            addErrorToResponseObj(responseOBj, userBan.login + " you have been banned for : " + userBan.reason, 14)
            res.send(JSON.stringify(responseOBj))
        }

        //console.log("\x1b[41m%s\x1b[0m", '.... End Authentification Middleware Function');

        next(); /// si aucune erreur n'est survenue avant, on execute la requete ici 
    }
}