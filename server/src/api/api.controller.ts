import { Controller, Get, Header, Res } from '@nestjs/common';
import { ApiService } from './api.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('api')
export class ApiController {
  constructor(private readonly apiService: ApiService) { }

  @Get()
  @Header('Content-Type', 'text/html; charset=utf-8')
  getApiHtml(@Res() res) {
    return res.sendFile('index.html', { root: './api_doc_sdk' });
  }

  @Get('/version')
  getVersion(): string {
    return this.apiService.getVersion(initResponse({}));
  }

  @Get('/cgu')
  getCgu(): string {
    return this.apiService.getCgu(initResponse({}));
  }

}
