import { Controller, Get, Header, Res, Param } from '@nestjs/common';
import { SdkService } from './sdk.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('/api/sdk')
export class SdkController {
  constructor(private readonly sdkService: SdkService) { }

  @Get()
  @Header('Content-Type', 'text/html; charset=utf-8')
  getSdkIndex(@Res() res) {
    return res.sendFile('sdk.html', { root: './api_doc_sdk' });
  }

  @Get('/:name')
  @Header('Content-Type', 'application/octet-stream')
  getSdk(@Res() res, @Param('name') name) {
    //console.log(name);
    return res.sendFile(name, { root: './api_doc_sdk' });
  }

}
