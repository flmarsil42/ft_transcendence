import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GameHistory } from './entities/gameHistory.entity'
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'
import { GameHistoryController } from './gameHistory.controller';
import { GameHistoryService } from './gameHistory.service';

@Module({
    imports: [TypeOrmModule.forFeature([GameHistory]), CacheModule.register({ ttl: 0 })],
    exports: [TypeOrmModule],
    controllers: [GameHistoryController],
    providers: [GameHistoryService]
})
export class GameHistoryModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthentificationMiddleware)
            .forRoutes(GameHistoryController);
    }
}
