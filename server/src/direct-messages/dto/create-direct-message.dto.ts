import { IsByteLength, IsDefined, IsNotEmpty, } from 'class-validator';
export class CreateDirectMessageDto {
    @IsDefined()
    @IsNotEmpty()
    usernameSource: string;

    @IsDefined()
    @IsNotEmpty()
    usernameTarget: string;

    @IsDefined()
    @IsByteLength(1, 255)
    message: string;

}