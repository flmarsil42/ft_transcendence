import { PickType } from "@nestjs/mapped-types";
import { IsBoolean, IsDefined, isDefined, IsNumber } from "class-validator";
import { CreateDirectMessageThreadDto } from "./create-direct-message-thread.dto";

export class UpdateDirectMessageThreadDto extends PickType(CreateDirectMessageThreadDto, ['lastMessageSeen', 'id'] as const) {
    @IsDefined()
    @IsNumber()
    id: number;
}