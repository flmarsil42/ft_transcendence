/*
** the directMessageThreads entity contains the id of the thread in which we
** can find all the past direct messages between two given users.
*/

import { Entity, UpdateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, } from 'typeorm';
import { User } from "../../users/entities/user.entity"

@Entity({ name: "direct_message_threads" })
export class DirectMessageThreads extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    //to see the last sent message time...
    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    lastEmitterUsername: string;

    @Column()
    lastEmitterLogin: string;

    @Column({ default: false })
    lastMessageSeen: boolean;

    @ManyToOne((type) => User, (user) => user.directMessageThreadAsUser1, {
        onDelete: 'CASCADE', //if we delete a user, the whole row related in this table are deleted!
    })
    user1: User;

    @ManyToOne((type) => User, (user) => user.directMessageThreadAsUser2, {
        onDelete: 'CASCADE', //if we delete a user, the whole row related in this table are deleted!
    })
    user2: User;
}