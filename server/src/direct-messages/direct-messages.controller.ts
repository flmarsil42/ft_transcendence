import { Body, Controller, Get, Post, Put, Query, Res, } from '@nestjs/common';
import { query } from 'express';
import { addErrorToResponseObj, initResponse, ResponseOBJ, } from '../utils-dto/utils.dto';
import { DirectMessagesService } from './direct-messages.service';
import { CreateDirectMessageDto } from './dto/create-direct-message.dto';
import { UpdateDirectMessageThreadDto } from './dto/update-direct-message-thread.dto';

@Controller('api/v1/directMessages')
export class DirectMessagesController {
  constructor(private readonly directMessagesService: DirectMessagesService) { }

  //this route/function will get the list of most recent directMessagesThreads
  //sets error 0
  @Get('threads')
  async getListOfDirectMessageThreads(@Query() query): Promise<string> {
    const responseObj: ResponseOBJ = initResponse(query);
    //TODO: can be removed after testing, since the login is always added to query...
    if (!query.hasOwnProperty('login')) {
      addErrorToResponseObj(responseObj, "login parameter missing.", 0);
      return JSON.stringify(responseObj);
    }
    if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
      addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 0);
      return JSON.stringify(responseObj);
    }
    return await this.directMessagesService.getListOfDirectMessageThreads(responseObj, query);
  }

  //this route/function will update a given thread to "lastMessageSeen === true".
  //sets errors: 0, 1, 2
  @Put('thread')
  async directMessageThreadUpdateOne(@Query() query, @Body() updateDirectMessageThreadDto: UpdateDirectMessageThreadDto): Promise<string> {
    //TODO: can be removed after testing, since the login is always added to query...
    if (!query.hasOwnProperty('login')) {
      const responseObj: ResponseOBJ = initResponse(query);
      addErrorToResponseObj(responseObj, "login parameter missing.", 0);
      return JSON.stringify(responseObj);
    }
    return await this.directMessagesService.directMessageThreadUpdateToSeen(initResponse(query), query, updateDirectMessageThreadDto);
  }

  //this route/function will get the list of most recent directMessages within a directMessagesThread for login
  //sets error 0, 1, 2, 3
  @Get('thread')
  async getListOfDirectMessagesInThread(@Query() query): Promise<string> {
    const responseObj: ResponseOBJ = initResponse(query);
    //TODO: can be removed after testing, since the login is always added to query...
    if (!query.hasOwnProperty('login')) {
      addErrorToResponseObj(responseObj, "login parameter missing.", 0);
      return JSON.stringify(responseObj);
    }
    if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
      addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 0);
      return JSON.stringify(responseObj);
    }
    if (!query.hasOwnProperty('threadId')) {
      addErrorToResponseObj(responseObj, "threadId property is missing.", 1);
      return JSON.stringify(responseObj);
    }

    return await this.directMessagesService.getListOfDirectMessagesInThread(responseObj, query);
  }

  //this route/function will get answer if a thread exists or not between the user requesting and a username.
  //sets error 0, 1, 2, 3
  @Get('threadExists')
  async getDirectMessagesThreadExists(@Query() query): Promise<string> {
    const responseObj: ResponseOBJ = initResponse(query);
    //TODO: can be removed after testing, since the login is always added to query...
    if (!query.hasOwnProperty('login')) {
      addErrorToResponseObj(responseObj, "login parameter missing.", 0);
      return JSON.stringify(responseObj);
    }
    if (!query.hasOwnProperty('username')) {
      addErrorToResponseObj(responseObj, "username parameter missing.", 1);
      return JSON.stringify(responseObj);
    }
    return await this.directMessagesService.getDirectMessagesThreadExists(responseObj, query);
  }

  //TODO REMOVE, TMP ROUTE FOR DEV
  //to create a directMessageThread between two users
  @Post('thread')
  async directMessageThreadCreateOne(@Query() query): Promise<string> {
    const responseObj: ResponseOBJ = initResponse(query);
    if (!query.hasOwnProperty('usernameSource') || !query.hasOwnProperty('usernameTarget')) {
      addErrorToResponseObj(responseObj, "usernameSource or usernameTarget parameter missing.", 0);
      return JSON.stringify(responseObj);
    }
    return await this.directMessagesService.directMessageThreadCreateOne(initResponse(query), query);
  }

  //TODO REMOVE, TMP ROUTE FOR DEV
  //must be called from the handling of a websocket event...
  //action: creates a direct_message entry, and eventually adds also
  //     a directMessageThread entry in db.
  //sets errors id: 0, 1, 2
  @Post('directMessage')
  async directMessageCreateOne(@Query() query, @Body() createDirectMessageDto: CreateDirectMessageDto): Promise<string> {
    return await this.directMessagesService.directMessageCreateOne(initResponse(query), createDirectMessageDto);
  }
}