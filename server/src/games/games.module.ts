import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { GamesService } from './games.service';
import { GamesController } from './games.controller';
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [CacheModule.register({ ttl: 0 })],
  controllers: [GamesController],
  providers: [GamesService]
})
export class GamesModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(GamesController);
  }
}
