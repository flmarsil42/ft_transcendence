import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Like, QueryBuilder, Repository } from 'typeorm';
import { User } from '../users/entities/user.entity';
import { Friend, FriendState } from './entities/friend.entity';
import { getConnection, Not } from "typeorm";
import { addErrorToResponseObj, addInternalServerErrorToResponseObj, ResponseOBJ } from '../utils-dto/utils.dto';
import { UsersController } from '../users/users.controller';

@Injectable()
export class FriendsService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Friend)
        private friendRepository: Repository<Friend>,
    ) { }

    //friends

    //obtenir la liste d'amis
    async getListOfFriends(responseObj: ResponseOBJ, query): Promise<string> {
        let liste = []
        if (query['index'] as number < 1) {
            query['index'] = 1;
        }
        await Friend.find({
            relations: ["users",],
            where: [
                { user_first_login: query['login'], state: FriendState.FRIEND },
                { user_second_login: query['login'], state: FriendState.FRIEND },
            ],
            order: {
                user_first_login: "ASC",
                user_second_login: "ASC"
            },
            skip: query['index'] as number === 0 ? 0 : ((10 * query['index'] as number) - 10),
            take: 10,
        }
        ).then(res => {
            res.forEach(element => {
                element.users.forEach(user => {
                    if (user.login != query['login']) {
                        liste.push({
                            'login': user.login,
                            'username': user.username,
                            'avatar': user.avatar,
                            'wins': user.wins,
                            'loses': user.loses,
                            'ratio': user.ratio,
                            'xp': user.xp,
                            'lvl': user.lvl,
                            'status': user.status,
                            'playingCurrently': user.playingCurrently,
                            'created_at': element.created_at, id: element.id
                        })
                    }
                })
            })
            responseObj.data = liste;
            //TODO
            //console.log("list of friends:\n", liste);
        })
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }

    //obtenir la liste de demande d'amitié
    async getContactRequest(responseObj: ResponseOBJ, query): Promise<string> {
        let liste = []
        if (query['index'] as number < 1) {
            query['index'] = 1;
        }
        await Friend.find({
            relations: ["users",],
            where: [
                { user_first_login: query['login'], state: FriendState.PENDING_SECOND_TO_FIRST },
                { user_second_login: query['login'], state: FriendState.PENDING_FIRST_TO_SECOND },
            ],
            order: {
                user_first_login: "ASC",
                user_second_login: "ASC"
            },
            skip: query['index'] as number === 0 ? 0 : ((10 * query['index'] as number) - 10),
            take: 10,
        }
        ).then(res => {
            res.forEach(element => {
                element.users.forEach(user => {
                    if (user.login != query['login']) {
                        liste.push({
                            'username': user.username,
                            'avatar': user.avatar,
                            'created_at': element.created_at, id: element.id
                        })
                    }
                })
            })
            responseObj.data = liste;
        })
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }


    async searchFriend(responseObj: ResponseOBJ, query): Promise<string> {
        /* let liste = []
         await Friend.find({
             relations: ["users",],
             where: [
                 { user_first_login: query['login'], user_second_login: Like(`%${query['username']}%`), state: FriendState.FRIEND },
                 { user_first_login: Like(`%${query['username']}%`), user_second_login: query['login'], state: FriendState.FRIEND },
             ],
             take: 10,
         }
         ).then(res => {
             res.forEach(element => {
                 element.users.forEach(user => {
                     if (user.login != query['login']) {
                         liste.push({
                             'username': user.username,
                             'avatar': user.avatar,
                             'created_at': element.created_at, id: element.id
                         })
                     }
                 })
             })
             responseObj.data = liste;
         })
             .catch((err) => {
                 //console.log('\x1b[36m%s\x1b[0m', err);
                 addInternalServerErrorToResponseObj(responseObj);
             });
*/
        addInternalServerErrorToResponseObj(responseObj);
        return JSON.stringify(responseObj);
    }

    //envoyer une demande d'amitié
    async setContactUser(responseObj: ResponseOBJ, query): Promise<string> {

        const myAccount = await User.findOne({
            where: [
                { 'login': query['login'] },
            ],
        })

        if (myAccount.username === query['username']) {
            addErrorToResponseObj(responseObj, "you may not ask as a friend yourself", 0);
            return JSON.stringify(responseObj)
        }

        const userContact = await User.findOne({
            where: [
                {
                    'username': query['username']
                },
            ],
        })

        if (userContact === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to be a friend is unknown", 1);
            return JSON.stringify(responseObj)
        }

        let user_first_login = ''
        let user_second_login = ''

        if (myAccount.login < userContact.login) {
            user_first_login = myAccount.login
            user_second_login = userContact.login
        } else {
            user_first_login = userContact.login
            user_second_login = myAccount.login
        }

        const resultFriend = await Friend.findOne({
            where: [
                {
                    'user_first_login': user_first_login,
                    'user_second_login': user_second_login,
                },
            ]
        })

        if (resultFriend === undefined) {
            let listeUser = [myAccount, userContact]

            const friend = new Friend()
            friend.user_first_login = user_first_login
            friend.user_second_login = user_second_login
            friend.users = listeUser

            if (user_first_login == myAccount.login) {
                friend.state = FriendState.PENDING_FIRST_TO_SECOND;
            } else {
                friend.state = FriendState.PENDING_SECOND_TO_FIRST;
            }
            await friend.save()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                    addInternalServerErrorToResponseObj(responseObj);
                });

        } else {
            if ((user_first_login === myAccount.login) && (resultFriend.state === FriendState.PENDING_SECOND_TO_FIRST)) {
                this.acceptFriend(responseObj, { login: myAccount.login, username: userContact.username }).then(res => {
                    return (res)
                })
            } else if ((user_second_login === myAccount.login) && (resultFriend.state === FriendState.PENDING_FIRST_TO_SECOND)) {
                this.acceptFriend(responseObj, { login: myAccount.login, username: userContact.username }).then(res => {
                    return (res)
                })
            }
        }

        return JSON.stringify(responseObj);
    }

    //supprimer une amitié
    async deleteContactUser(responseObj: ResponseOBJ, query): Promise<string> {
        //console.log("////-------******* :", query)
        const userContact = await User.findOne({
            where: [
                { 'username': query['username'] },
            ]
        })

        if (userContact === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to delete/refuse frienship with is unknown", 0);
            return JSON.stringify(responseObj)
        }


        let user_first_login = ''
        let user_second_login = ''

        if (query['login'] < userContact.login) {
            user_first_login = query['login']
            user_second_login = userContact.login
        } else {
            user_first_login = userContact.login
            user_second_login = query['login']
        }




        await Friend.createQueryBuilder()
            .delete()
            .where('user_first_login =:user_first_login AND  user_second_login =:user_second_login', { 'user_first_login': user_first_login, 'user_second_login': user_second_login })
            .execute()
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }

    //accepter une amitié
    async acceptFriend(responseObj: ResponseOBJ, query): Promise<string> {
        const userContact = await User.findOne({
            where: [
                { 'username': query['username'] },
            ],
        })

        if (userContact === undefined) {
            addErrorToResponseObj(responseObj, "the user you requested to be a friend is unknown", 0);
            return JSON.stringify(responseObj)
        }

        let user_first_login = ''
        let user_second_login = ''
        let state_request;

        if (query['login'] < userContact.login) {
            user_first_login = query['login']
            user_second_login = userContact.login
            state_request = FriendState.PENDING_SECOND_TO_FIRST;
        } else {
            user_first_login = userContact.login
            user_second_login = query['login']
            state_request = FriendState.PENDING_FIRST_TO_SECOND;
        }

        await Friend.createQueryBuilder()
            .update()
            .set({ state: FriendState.FRIEND })
            .where('user_first_login=:user_first_login AND user_second_login=:user_second_login AND  state =:state', {
                'user_first_login': user_first_login, 'user_second_login': user_second_login, state: state_request
            })
            .execute()
            .catch((err) => {
                //console.log('\x1b[36m%s\x1b[0m', err);
                addInternalServerErrorToResponseObj(responseObj);
            });
        return JSON.stringify(responseObj);
    }

    //refuser une amitié
    async refuseFriend(responseObj: ResponseOBJ, query): Promise<string> {
        return this.deleteContactUser(responseObj, query);
    }
}
