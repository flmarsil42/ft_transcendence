import { Module, RequestMethod, MiddlewareConsumer, CacheModule } from '@nestjs/common';
import { FriendsService } from './friends.service';
import { FriendsController } from './friends.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Friend } from './entities/friend.entity'
import { UsersModule } from '../users/users.module'
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'

@Module({
  imports: [TypeOrmModule.forFeature([Friend]), UsersModule, CacheModule.register({ ttl: 0 })],
  exports: [TypeOrmModule],
  controllers: [FriendsController],
  providers: [FriendsService]
})
export class FriendsModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(FriendsController);
  }
}