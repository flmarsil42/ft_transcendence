import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { Friend } from '../friends/entities/friend.entity';
import { ChatChannel } from '../chatChannels/entities/chatChannel.entity';
import { UserSession } from '../userSessions/entities/userSession.entity'
import { ChatChannelBannedOrMutedUser } from '../chatChannels/entities/chatChannelBannedOrMutedUser.entity';
import { GameHistory } from '../gameHistory/entities/gameHistory.entity';
import { DirectMessageThreads } from '../direct-messages/entities/direct-message-threads.entity';
import { DirectMessages } from '../direct-messages/entities/direct-messages.entity';
import { Ban } from '../admin/entities/ban.entity';
import { AppStats } from '../admin/entities/appStats.entity';
import { Game } from '../games/entities/game.entity';
import { Challenge } from '../games/entities/challenge.entity';
import { DoubleAuth } from '../userSessions/entities/doubleAuth.entity';

@Module({
    imports: [TypeOrmModule.forRoot({
        type: 'postgres',
        port: parseInt(<string>process.env.DB_PORT, 10),
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        synchronize: true,
        entities: [User, Friend, ChatChannel, UserSession, ChatChannelBannedOrMutedUser, GameHistory, DirectMessageThreads, DirectMessages, Ban, AppStats, Game, Challenge, DoubleAuth],
    })]
})
export class DatabaseModule { }
