import { Entity, UpdateDateColumn, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity, } from 'typeorm';


@Entity({ name: "doubleauth" })
export class DoubleAuth extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    //Must be included in the cookie.
    @Column({ unique: true })
    login: string;

    //Must be included in the cookie as well.
    @Column({ unique: true })
    code: string;

    @Column({ default: 4 })
    attempts: number;

    @CreateDateColumn()
    createdAt: Date

}