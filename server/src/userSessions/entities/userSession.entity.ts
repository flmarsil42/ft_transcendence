import { Entity, UpdateDateColumn, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity, } from 'typeorm';


@Entity({ name: "usersessions" })
export class UserSession extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    //Must be included in the cookie.
    @Column({ unique: true })
    login: string;

    //Must be included in the cookie as well.
    @Column({ unique: true })
    token: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

}